﻿using System;
using System.Linq;

namespace ConsoleApp1
{
    public class Program
    {        
        public static int[] RotateArray(int rotateBy, int[] input)
        {
            var left = input.Take(rotateBy);
            var right = input.Skip(rotateBy);
            return right.Concat(left).ToArray();            
        }
     
        public static string ToString(int[] value) =>
            string.Join(",", value.Select(x => x.ToString()).ToArray());
        
        public static bool SplitArray(int[] left, int[] right)
        {
            if (right.Any())
            {
                Console.WriteLine($"[{ToString(left)}] [{ToString(right)}]");
                if (left.Any())
                    if (left.Sum() == right.Sum())
                        return true;

                for(int i = 0; i < right.Length; i++)
                {
                    var rotatedRight = RotateArray(i, right);
                    if (SplitArray(rotatedRight.Take(left.Length+1).ToArray(), 
                            rotatedRight.Skip(left.Length+1).ToArray()))
                        return true;
                }                
            }
            return false;            
        }
        
                    
        public static bool SplitArray(int[] input)
        {
            return SplitArray(new int[] { }, input);
        }
        

        public static void Main(string[] args)
        {
            Console.WriteLine(SplitArray(new int[] { 1, 3, 3, 4, 5 }));
            Console.ReadKey();
        }
    }
}
